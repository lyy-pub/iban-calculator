package io.luyouyou.iban;

import java.math.BigInteger;
import java.util.concurrent.ThreadLocalRandom;

/** Calculates, generates, and formats IBAN. Only for demo purpose, calculations and numbers are not correct. */
public class PfIbanCalculator {

  private static final String COUNTRY_CODE = "CH";
  private static final String BANK_CODE = "09000";

  public String calculateIbanFromAccountNumber(final String accountNumber) {
    String[] accountNumberParts = accountNumber.split("-");
    String iban =
        COUNTRY_CODE
            + "00"
            + BANK_CODE
            + String.format("%05d", Integer.parseInt(accountNumberParts[0]))
            + String.format("%06d", Integer.parseInt(accountNumberParts[1]))
            + accountNumberParts[2];
    return replaceCheckDigits(iban);
  }

  public String calculateCheckDigits(final String iban) {
    final String unformattedIban = formatIbanRaw(iban);
    if (unformattedIban.length() != 21) {
      throw new IllegalArgumentException("IBAN must be 21 characters long");
    }
    if (!(unformattedIban.startsWith(COUNTRY_CODE) && unformattedIban.startsWith(BANK_CODE, 4))) {
      throw new java.lang.IllegalArgumentException("Calculation only works for PF-IBAN");
    }
    final String checkDigit =
        Integer.toString(
            98
                - (new BigInteger(unformattedIban.substring(5) + "121700")
                        .mod(BigInteger.valueOf(97)))
                    .intValue());
    return checkDigit.length() == 2 ? checkDigit : "0" + checkDigit;
  }

  public boolean areCheckDigitsCorrect(final String iban) {
    return iban.substring(2, 4).equals(calculateCheckDigits(iban));
  }

  public String replaceCheckDigits(final String iban) {
    return iban.substring(0, 2) + calculateCheckDigits(iban) + iban.substring(4);
  }

  public String formatIbanPretty(final String iban) {
    return formatIbanRaw(iban).replaceAll("(.{4})", "$1 ");
  }

  public String formatIbanRaw(final String iban) {
    return iban.replaceAll("\\s", "");
  }

  public String generatePseudoIban() {
    final String twelveDigitsRandom =
        Long.toString(ThreadLocalRandom.current().nextLong(1000000000000L, 10000000000000L))
            .substring(1);
    return replaceCheckDigits(COUNTRY_CODE + "00" + BANK_CODE + twelveDigitsRandom);
  }

  public String generatePseudoIbanPretty() {
    return formatIbanPretty(generatePseudoIban());
  }

  public String generatePseudoAccountNumber() {
    final String twoDigitsRandom =
        Integer.toString(ThreadLocalRandom.current().nextInt(100, 1000)).substring(1);
    return twoDigitsRandom
        + "-"
        + ThreadLocalRandom.current().nextInt(1000000)
        + "-"
        + ThreadLocalRandom.current().nextInt(10);
  }

  public static void main(String[] args) {
    String accountNumber = new PfIbanCalculator().generatePseudoAccountNumber();
    System.out.println(accountNumber);
    System.out.println(new PfIbanCalculator().calculateIbanFromAccountNumber(accountNumber));
  }
}
