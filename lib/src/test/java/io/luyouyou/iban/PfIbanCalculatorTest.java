package io.luyouyou.iban;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PfIbanCalculatorTest {
  PfIbanCalculator pfIbanCalculator;

  @BeforeAll
  void setup() {
    pfIbanCalculator = new PfIbanCalculator();
  }

  @Test
  public void testGeneratePseudoIban() {
    for (int i = 0; i < 100; i++) {
      String pseudoIban = pfIbanCalculator.generatePseudoIban();
      assertIsIban(pseudoIban);
    }
  }

  @Test
  public void testGeneratePseudoIbanPretty() {
    String pseudoIbanPretty = pfIbanCalculator.generatePseudoIbanPretty();
    assertIbanIsPretty(pseudoIbanPretty);
    assertTrue(pfIbanCalculator.areCheckDigitsCorrect(pseudoIbanPretty));
  }

  @Test
  public void testFormatIbanPretty() {
    String pseudoIbanPretty = pfIbanCalculator.formatIbanPretty(pfIbanCalculator.generatePseudoIban());
    assertIbanIsPretty(pseudoIbanPretty);
  }

  @Test
  public void testGeneratePseudoAccountNumber() {
    for (int i = 0; i < 100; i++) {
      assertTrue(pfIbanCalculator.generatePseudoAccountNumber().matches("^\\d{2}-\\d+-\\d$"));
    }
  }

  @Test
  public void testCalculateIbanFromAccountNumber() {
    String iban = pfIbanCalculator.calculateIbanFromAccountNumber("43-198264-6");
    assertIsIban(iban);
    assertEquals("CH4409000000431982646", iban);
  }

  private void assertIsIban(final String iban) {
    assertTrue(iban.matches("^CH\\d{19}$"));
    assertTrue(pfIbanCalculator.areCheckDigitsCorrect(iban));
  }

  private void assertIbanIsPretty(final String iban) {
    assertTrue(iban.matches("^CH\\d{2}( \\d{4}){4} \\d$"));
  }
}
